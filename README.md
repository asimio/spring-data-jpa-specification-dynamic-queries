# README #

Accompanying source code for blog entries at:

- https://tech.asimio.net/2020/11/21/Implementing-dynamic-SQL-queries-using-Spring-Data-JPA-Specification-and-Criteria-API.htm 
- https://tech.asimio.net/2021/01/27/Troubleshooting-Spring-Data-JPA-Specification-and-Criteria-queries-impact-on-Hibernate-QueryPlanCache.html


### Requirements ###

* Java 8+
* Maven 3.2.x+

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/films"
curl "http://localhost:8080/api/films?minRentalRate=0.5&maxRentalRate=4.99"
curl "http://localhost:8080/api/films?releaseYear=2006""
curl "http://localhost:8080/api/films?category=Action&category=Comedy&category=Horror&minRentalRate=0.99&maxRentalRate=4.99&releaseYear=2005"
curl "http://localhost:8800/api/films/{id}"   (1, for instance)
```
should result in successful responses. Please look at the logs to verify Hibernate executes different queries depending on the request parameters.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
